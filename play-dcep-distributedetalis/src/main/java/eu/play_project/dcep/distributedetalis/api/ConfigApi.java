package eu.play_project.dcep.distributedetalis.api;


/**
 * Set a configuration for a dEtalis instance.
 * 
 * @author Stefan Obermeier
 */
public interface ConfigApi {
	
	public void setConfig(Configuration configuration) throws DistributedEtalisException;
}
