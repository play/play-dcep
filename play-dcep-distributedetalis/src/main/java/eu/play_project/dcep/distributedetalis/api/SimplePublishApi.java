package eu.play_project.dcep.distributedetalis.api;

import fr.inria.eventcloud.api.CompoundEvent;

public interface SimplePublishApi {

	public void publish(CompoundEvent event);
	
}
